package com.zipkin.viacep.controller;

import com.zipkin.viacep.externo.ViaCepRetorno;
import com.zipkin.viacep.service.ViaCepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ViaCepController {

    @Autowired
    private ViaCepService viaCepService;

    @GetMapping("/viacep/{cep}")
    public ViaCepRetorno buscar(@PathVariable String cep){
        return viaCepService.buscar(cep);
    }
}
