package com.zipkin.viacep.service;

import com.zipkin.viacep.externo.ViaCepRetorno;
import com.zipkin.viacep.externo.ViacepFeing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ViaCepService {

    @Autowired
    private ViacepFeing viacepFeing;

    public ViaCepRetorno buscar(String cep){
        return viacepFeing.buscar(cep);
    }
}
