package com.zipkin.viacep.externo;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "viacep", url = "viacep.com.br")
public interface ViacepFeing {

    @GetMapping("/ws/{cep}/json/")
    ViaCepRetorno buscar(@PathVariable String cep);
}
